#!/usr/bin/env python3

"""Core functionality of the Incoinco Telegram bot"""

import argparse
import json
import logging
import logging.handlers
import os
import threading
import time

import requests
from retry import retry

import rhymes

DIR = os.path.dirname(os.path.realpath(__file__))


class TelegramMessageUpdate:
    """Represents a Message object, see https://core.telegram.org/bots/api#message"""

    def __init__(self, update_json):
        self.id = update_json["update_id"]

        message = update_json["message"]

        self.chat_id = message["chat"]["id"]

        # Messages can contain photos, audio, etc. We only want the text
        self.text = message.get("text", None)


class TelegramBotInterface:
    def __init__(self, token):
        self.base_url = f"https://api.telegram.org/bot{token}"

        # When getting new updates from Telegram, we specify an "offset", which
        # is the update ID of the last received update + 1. This way, we won't
        # get the same updates again. Initially, this is 0
        self.last_update_id = 0

    def get_updates(self):
        """Returns a list of updates received from Telegram"""

        # Build payload for /getUpdates request (see
        # https://core.telegram.org/bots/api#getupdates). For now there is no
        # need to make these settings configurable

        # Timeout when waiting for updates (long polling)
        update_timeout_seconds = 3600

        # allowed_updates restricts the type of updates. We are only interested
        # in messages
        allowed_updates = ["message"]

        params = {
            "allowed_updates": allowed_updates,
            "offset": self.last_update_id + 1,
            "timeout": update_timeout_seconds,
        }

        try:
            response = self._make_request("GET", "/getUpdates", params=params)
        except requests.RequestException as e:
            logging.error("Error while getting updates: %s", e)
            return []

        try:
            response = response.json()
        except json.decoder.JSONDecodeError as e:
            logging.error("Failed to parse response as JSON: %s", e)

        # Check the "ok" field, see https://core.telegram.org/bots/api#making-requests
        if not response["ok"]:
            logging.error(
                "Telegram indicated that the request was not successful: %s",
                response["description"],
            )
            return []

        logging.debug("Received response: %s", response)

        # The actual result is stored in the "result" field (see
        # https://core.telegram.org/bots/api#making-requests), and in the case
        # of the getUpdates method, it contains an array of "Update" objects
        # (see https://core.telegram.org/bots/api#getupdates)
        update_list = [
            TelegramMessageUpdate(update_json) for update_json in response["result"]
        ]

        # If there were actual updates (we could get an empty list), store the
        # ID of the last one, to take it as a base when setting the offset the
        # next time this method is called
        if len(update_list) > 0:
            self.last_update_id = update_list[-1].id
            logging.debug("Last update ID changed to %s", self.last_update_id)

        return update_list

    def send_message(self, chat_id, text):
        params = {"chat_id": chat_id, "text": text}
        self._make_request("POST", "/sendMessage", params=params)

        logging.debug("Sent message: %s to chat ID %s", text, chat_id)

    @retry(requests.RequestException, delay=1, backoff=1.5, tries=10)
    def _make_request(self, method, path, params=None):
        logging.debug("Making a %s request to %s with params: %s", method, path, params)
        response = requests.request(method, self.base_url + path, params=params)
        response.raise_for_status()
        return response


def get_token():
    return os.getenv("INCOINCO_TOKEN", None)


def parse_args():
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("--log-level", default=logging.INFO)
    parser.add_argument(
        "--rhymes-file",
        default=os.path.join(DIR, "rhymes.json"),
        help="JSON file containing a set of regexes and corresponding rhymes",
    )
    parser.add_argument(
        "--rudeness-level",
        choices=[0, 1, 2],
        default=2,
        type=int,
        help="Rudeness level of rhymes. 0 only responds with kid-friendly "
        "rhymes, 1 is intermediate, 2 is the rudest.",
    )

    return parser.parse_args()


def setup_logging(log_level):
    log_format = "%(name)s(%(levelname)s)[%(asctime)s]: %(message)s"
    logging.basicConfig(level=log_level, format=log_format)

    logging.debug("Logging setup successfully")


def main():
    """Entry point to the bot program, essentialy: setup + forever loop"""

    args = parse_args()

    setup_logging(args.log_level)

    token = get_token()
    if not token:
        raise RuntimeError("Token not found")
    logging.debug("Token read successfully")

    telegram = TelegramBotInterface(token)
    rhymer = rhymes.Rhymer(args.rhymes_file, args.rudeness_level)

    while True:
        updates = telegram.get_updates()
        logging.debug("Received %s updates", len(updates))

        for update in updates:
            logging.debug("Received update from chat ID: %s", update.chat_id)

            if update.text is None:  # e.g. someone sent a voice message
                logging.debug("This update does not contain text, skipping")
                continue

            # Check if the text matches any rhyme, and reply back if it does
            rhyme = rhymer.rhyme(update.text)

            if not rhyme:
                logging.debug("Not rhyming")
                continue

            logging.info(
                'Rhyming back! Message: "...%s" matched rhyme: "%s"',
                update.text.rsplit()[-1],
                rhyme,
            )
            try:
                telegram.send_message(update.chat_id, rhyme)
            except requests.RequestException as e:
                logging.warning("Failed to send message: %s", e)


if __name__ == "__main__":
    main()
