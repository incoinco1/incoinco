# Incoinco

Telegram bot that sends back rhymes. It reads messages sent to it, or sent to a
group it is part of, and sends rhymes if appropiate. **This means that the bot
has to be able to read all messages!**

Whether or not to rhyme is decided by matching the end of each message against
a set of regexes, each of which has a rhyme associated to them.


## Running the bot
1. Create a bot and get the token ([instructions](https://core.telegram.org/bots#3-how-do-i-create-a-bot))
2. Install requirements
  ```bash
  (venv) $ pip install -r requirements.txt
  ```
3. Set the INCOINCO_TOKEN environment variable to your bot's token, and start the bot
  ```bash
  INCOINCO_TOKEN=<the token> ./incoinco.py
  ```


## Configuration options

Please run `./incoinco.py --help` for a description of the available options.


## Rhymes

The program expects a JSON file with a list of regexes and the corresponding
rhymes. An example can be seen in the [rhymes.json](./rhymes.json) file, which
is also the default rhymes file used if a different one is not specified in the
command line.
