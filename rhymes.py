# -*- coding: utf-8 -*-

"""Functions related to rhyming for use by the Incoinco Telegram bot"""

import json
import logging
import os
import random
import re
import unicodedata

# Rhyme regex end. This enables us to match rhymes even if the user writes
# other non-phonetic characters in the end. Allowing any non-alphanumeric
# character is cleaner but probably not the best option (for example
# '5!£$&*][{¾€³' will match 5...)
# See https://stackoverflow.com/a/25978940 for details on currency symbols
CURRENCY_SYMBOLS = u"".join(
    chr(i) for i in range(0xFFFF) if unicodedata.category(chr(i)) == "Sc"
)
RHYME_REGEX_END = "[^a-zA-Z0-9%s]*$" % CURRENCY_SYMBOLS


class Rhymer:
    def __init__(self, rhymes_file, rudeness_level):
        with open(rhymes_file) as fd:
            self.rhymes = json.load(fd)
        self.rudeness_level = rudeness_level
        logging.debug(
            "Initialized Rhymer with file %s and level %s", rhymes_file, rudeness_level
        )

    def rhyme(self, text):
        if is_link(text):
            logging.debug("The text (%s) seems to be a link, not rhyming", text)
            return

        text = text.upper()

        # Get first rhyme that matches. Iterate in random order to avoid getting always the same one
        random.shuffle(self.rhymes)
        for rhyme in self.rhymes:
            if rhyme["level"] > self.rudeness_level:
                logging.debug(
                    'Level of rhyme "%s" is higher (%s) than configured (%s), skipping',
                    rhyme["rhyme"],
                    rhyme["level"],
                    self.rudeness_level,
                )
                continue
            regex, frequency = rhyme["regex"], rhyme["frequency"]
            if not re.search(regex + RHYME_REGEX_END, text):
                logging.debug("Regex %s did not match text %s", regex, text)
                continue
            if not should_rhyme(frequency):
                logging.debug(
                    "Regex %s matched text %s but throttled down", regex, text
                )
                continue

            last_word_text = text.rsplit(None, 1)[-1]
            last_word_rhyme = rhyme["rhyme"].upper().rsplit(None, 1)[-1]
            if last_word_rhyme == last_word_text:
                logging.debug(
                    "Last words of text and rhyme are equal (%s), skipping",
                    last_word_text,
                )
                continue

            logging.debug('Found rhyme "%s" for text "%s"', rhyme["rhyme"], text)
            return rhyme["rhyme"]

        logging.debug("No rhymes matched text %s", text)
        return None


def is_link(text):
    """Check if the passed text contains a hyperlink"""

    if re.search("http", text):
        return True
    return False


def should_rhyme(frequency):
    """Decide if rhyming should be aborted

    Sometimes the bot gets a bit tiring. Reduce the frequency of its responses
    according to each rhyme's "frequency" field stored in the database. A
    frequency of 0 will block all responses while a frequency of 100 will
    always rhyme back"""

    if frequency > 100 * random.random():
        return True
    return False
